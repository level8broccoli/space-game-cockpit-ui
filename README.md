# Space Game Cockpit UI 

## Inspiration & Resources

- https://andy-bell.co.uk/a-more-modern-css-reset/ 
- https://web.dev/articles/building/a-color-scheme
- https://monaspace.githubnext.com/
- https://utopia.fyi
- https://arwes.dev
- https://bulma.io